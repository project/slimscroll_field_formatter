SUMMARY - Slimscroll Field Formatter
===================================

Sliomscroll is small drupal module which can be added through the field API to
add type of text area.


Installation:
-------------

Install this module as usual. Please see
http://drupal.org/documentation/install/modules-themes/modules-8

Download Slimscroll library https://github.com/rochal/jQuery-slimScroll/releases

Put the downloaded library inside libraries directory & structure would be

/libraries/slimScroll/jquery.slimscroll.min.js


Configuration & Usage:
----------------------

Go to Entity Managed Display section & select the 'Slim Scroll' Formatter for
textarea field.

More Details on Options: http://rocha.la/jQuery-slimScroll/

Support:
--------
https://www.drupal.org/u/vedprakash
